output "volume_id" {
	value = "${aws_ebs_volume.waasup.id}"
}

resource "aws_ebs_volume" "waasup" {
	size = 10
	availability_zone = "${var.region}a"

	tags {
		Name = "waasup"
	}
}
