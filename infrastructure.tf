output "ip" {
	value = "${aws_instance.workstation.public_ip}"
}

resource "aws_volume_attachment" "storage" {
	device_name = "/dev/sdh"
	volume_id   = "${var.volume_id}"
	instance_id = "${aws_instance.workstation.id}"
}

variable "volume_id" {
	type = "string"
}

resource "aws_instance" "workstation" {
	ami               = "${var.ami}"
	instance_type     = "m5.large"
	key_name          = "${aws_key_pair.workstation.key_name}"
	security_groups   = ["${aws_security_group.workstation.name}"]
	availability_zone = "${var.region}a"
	user_data         = "${data.local_file.disk_setup.content}"

	instance_initiated_shutdown_behavior = "terminate"

	tags {
		Name = "waasup"
	}
}

variable "ami" {
	type = "string"
}

resource "aws_key_pair" "workstation" {
	key_name   = "waasup"
	public_key = "${data.local_file.pubkey.content}"
}

data "local_file" "pubkey" {
	filename = "${var.pubkey}"
}

variable "pubkey" {
	type = "string"
}

resource "aws_security_group" "workstation" {
	name        = "waasup"
	description = "Allows SSH traffic into the waasup host only from wherever you ran make waasup"

	ingress {
		from_port   = 22
		to_port     = 22
		protocol    = "tcp"
		cidr_blocks = ["${var.natip}/32"]
	}

	egress {
		# Let this box reach all of IP4 space
		from_port   = 0 
		to_port     = 0
		protocol    = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

variable "natip" {
	type = "string"
}

data "local_file" "disk_setup" {
	filename = "disk_setup.sh"
}
