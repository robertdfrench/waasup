SHELL=bash

## Overrideable Paramters
PUBKEY=$(shell eval echo "~$(shell whoami)/.ssh/id_rsa.pub")
REGION="us-east-1"

## External Tools
packer     = $(shell which packer     || echo ".missing.packer")
terraform  = $(shell which terraform  || echo ".missing.terraform")
jq         = $(shell which jq         || echo ".missing.jq")
curl       = $(shell which curl       || echo ".missing.curl")
ssh        = $(shell which ssh        || echo ".missing.ssh")
ssh-add    = $(shell which ssh-add    || echo ".missing.ssh-add")
ssh-keygen = $(shell which ssh-keygen || echo ".missing.ssh-keygen")
.missing.%:
	$(error You need to install $*)

## "Utility" Macros
Mkdir = mkdir -p $$(dirname $(1))
Touch = $(call Mkdir, $(1)); touch $(1)

## Targets
waasup: $(ssh) $(ssh-add) terraform.tfstate
	$(ssh-add)
	$(ssh) -o ForwardAgent=yes ec2-user@$$(terraform output ip)

terraform.tfstate: $(terraform) $(curl) build/tfinit build/tfvars.json infrastructure.tf $(PUBKEY) build/storage.tfstate disk_setup.sh
	$(terraform) apply \
		-var-file="build/tfvars.json" \
		-var "region=$(REGION)" \
		-var "pubkey=$(PUBKEY)" \
		-var "natip=$$(curl http://ifconfig.me/ip)" \
		-var "volume_id=$$($(terraform) output -state=build/storage.tfstate volume_id)"

build/tfinit: $(terraform)
	$(terraform) init
	$(call Touch, $@)

build/tfvars.json: $(jq) build/image_manifest.json
	$(call Mkdir, $@)
	$(jq) '{ami: .builds[-1:][0].artifact_id | split(":")[1]}' build/image_manifest.json > $@

build/image_manifest.json: $(packer) image.json build/valid_image_definition
	$(call Mkdir, $@)
	$(packer) build \
		-var "output=$@" \
		-var "region=$(REGION)" \
	       	image.json

build/valid_image_definition: $(packer) image.json
	$(packer) validate image.json
	@$(call Touch, $@)

build/storage.tfstate: $(terraform) storage/*.tf
	$(call Mkdir, $@)
	$(terraform) apply \
		-var "region=$(REGION)" \
		-state=$@ \
		storage

$(PUBKEY):
	$(info You will be prompted to create an SSH keypair. Follow all the)
	$(info instructions, and take the default options when prompted.)
	ssh-keygen
