# WaaSup
*Workstation-as-a-Service for the Unix Professional*

WaaSup will spin up a beefier workstation so you can do real work, unlike what
you have been doing from your cheap, underpowered laptop. To get started, simply
type `make waasup` from inside this repository.

### Requirements
WaaSup depends on these outstanding open source tools:

* [Terraform](https://www.terraform.io)
* [Packer](https://www.packer.io/)
* [Make](https://www.gnu.org/software/make/)
* [jq](https://stedolan.github.io/jq/)

Get them from your package manager of choice.
