#!/bin/bash -ex
device=/dev/$(lsblk -s --json | jq -r '.blockdevices | map(select(has("children") | not))[0].name')
if [ $(file -s $device | cut -f2 -d' ') -eq "data" ]; then
	mkfs -t ext4 $device
fi

keyfile="/home/ec2-user/.ssh/authorized_keys"
keys=$(cat $keyfile)
mount $device /home
mkdir -p $(dirname $keyfile)
touch $keyfile
grep -q -F "$keys" $keyfile || echo "$keys" >> $keyfile
chown -R ec2-user:ec2-user /home/ec2-user
